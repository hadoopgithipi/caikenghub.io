---
layout: post
title:  Flask 无法获取请求头数据
categories: 
    - Brandon
tags: 
    - Python
    - Flash
    - Http
prism: true
---

__问题描述:__ 使用如下代码获取请求头中的数据，拿不到值（客户端也使用 http_token）
```python
http_token=request.header.get_all("http_token")
```

__解决步骤:__

1. 打印请求头中的数据

	```python
	print(request.headers)
	```
 
	发现客户端无论使用 `http-token` 还是 `http_token` 请求头在服务端都变成了"Http-Token"。

2. 在服务端修改，使用如下代码获取请求头中的数据，问题解决

```python
http_token=request.header.get_all("http-token")
```


__总结:__

看来是 Flask 自动把请求头的格式修改了。