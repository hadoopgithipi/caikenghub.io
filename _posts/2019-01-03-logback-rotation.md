---
layout: post
title:  Logback 日志按天轮换问题
categories: 
    - Brandon
tags: 
    - Java
    - Logback
prism: true
---

Java 项目，使用 Logback 按天轮换日志，配置如下所示，问题是有时无法看到前一天生成的日志。
```xml
<appender name="INFO_LOG_FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <file>${LOG_PATH}/info.log</file>
        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <fileNamePattern>${LOG_PATH}/info/{yyyy-MM-dd}_info.log</fileNamePattern>
            <maxHistory>30</maxHistory>
        </rollingPolicy>
        <append>true</append>
        <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
            <Pattern>${format}</Pattern>
            <charset>utf8</charset>
        </encoder>
        <filter class="ch.qos.logback.classic.filter.LevelFilter">
            <level>info</level>
            <onMatch>ACCEPT</onMatch>
            <onMismatch>DENY</onMismatch>
        </filter>
    </appender>
```

#### 解决步骤

1. 网上搜索答案
	百思不得其解

2. 查看源码

	2.1 查看日志策略 `ch.qos.logback.core.rolling.TimeBasedRollingPolicy`
	发现其内置了一个策略触发器:
	```java
	TimeBasedFileNamingAndTriggeringPolicy<E> timeBasedFileNamingAndTriggeringPolicy;
	```
	且将其设置为默认的触发策略:
	```java
	if (timeBasedFileNamingAndTriggeringPolicy == null) {
        timeBasedFileNamingAndTriggeringPolicy = new DefaultTimeBasedFileNamingAndTriggeringPolicy<E>();
    }
	```
	2.2 查看默认的日志出发策略 `DefaultTimeBasedFileNamingAndTriggeringPolicy`
	其中的 `isTriggeringEvent` 方法实现了日志轮换的逻辑：
	```java
    public boolean isTriggeringEvent(File activeFile, final E event) {
        long time = getCurrentTime();
        if (time >= nextCheck) {
            Date dateOfElapsedPeriod = dateInCurrentPeriod;
            addInfo("Elapsed period: " + dateOfElapsedPeriod);
            elapsedPeriodsFileName = tbrp.fileNamePatternWithoutCompSuffix.convert(dateOfElapsedPeriod);
            setDateInCurrentPeriod(time);
            computeNextCheck();
            return true;
        } else {
            return false;
        }
    }
	```
	2.3 通过查看函数调用，发现 __只有在有新的日志写入时，才判断是否轮换日志__ 。
	`ch.qos.logback.core.rolling.RollingFileAppender` :
	```java
	@Override
    protected void subAppend(E event) {
        // The roll-over check must precede actual writing. This is the
        // only correct behavior for time driven triggers.
        // We need to synchronize on triggeringPolicy so that only one rollover
        // occurs at a time
        synchronized (triggeringPolicy) {
            if (triggeringPolicy.isTriggeringEvent(currentlyActiveFile, event)) {
                rollover();
            }
        }
        super.subAppend(event);
    }
	```

#### 补充

那么有没有定时生成日志的配置？
效果是：到了0点必须生成前一天的日志。
