---
layout: post
title:  java mysql 插入数据 阻塞
categories: 
    - Brandon
tags: 
    - Java
    - Mysql
prism: true
---

Java 项目，使用 MyBatis 向 MySQL 中插入数据时，线程阻塞住了，很长时间都不返回。

#### 解决步骤

1. 使用客户端工具登入mysql

2. 查看正在执行的语句

	```sql
	show processlist;
	```

	显示如下信息:

	Id|User|Host|db|Command|Time|State|Info
	-|-|-|-|-|-|-|-
	*|*|*|*|Query|131|Starting|Commit

3. 搜索上述信息的含义：

	* Command=Query 表示有语句正在执行；
	* State=Starting 官方文档没有介绍
	* Info=Commit 官方文档没有介绍

	看完并没有什么头绪。

4. 鼓捣了半天，发现是磁盘满了, 想删除一些没用数据，结果还是不行。因为连undo日志都没地方放了。

5. 磁盘扩容后一切正常。


#### 补充

* 好奇怪，没有超时时间吗，程序一直阻塞着。