---
layout: post
title:  ffmpeg 转换 mp4 自定义start的值
categories: 
    - Brandon
tags: 
    - ffmpeg
    - hls
prism: true
---

__问题描述:__
一段 mp4 视频由很多片段组成，使用 ffmpeg 将各个 mp4 片段文件转换为 ts 文件，之后使用 hls 协议播放一整段视频。发现进度条一直在前几秒循环式地跳。

__解决步骤:__

1. 从网上找了一段正常的视频，使用 ffmpeg 对比了正常的视频各个 ts 文件的元数据和本地不正常的视频各个 ts 文件的元数据。
发现正常视频片段的 start 值是连续的，不正常的视频片段的 start 值都是固定的，由此认为 start 决定了进度条的初始位置。

	```shell
	ffmpeg version 4.1-tessus  https://evermeet.cx/ffmpeg/  Copyright (c) 2000-2018 the FFmpeg developers
	  built with Apple LLVM version 10.0.0 (clang-1000.11.45.5)
	  configuration: --cc=/usr/bin/clang --prefix=/opt/ffmpeg --extra-version=tessus --enable-avisynth --enable-fontconfig --enable-gpl --enable-libaom --enable-libass --enable-libbluray --enable-libfreetype --enable-libgsm --enable-libmodplug --enable-libmp3lame --enable-libmysofa --enable-libopencore-amrnb --enable-libopencore-amrwb --enable-libopus --enable-librubberband --enable-libshine --enable-libsnappy --enable-libsoxr --enable-libspeex --enable-libtheora --enable-libtwolame --enable-libvidstab --enable-libvo-amrwbenc --enable-libvorbis --enable-libvpx --enable-libwavpack --enable-libx264 --enable-libx265 --enable-libxavs --enable-libxvid --enable-libzimg --enable-libzmq --enable-libzvbi --enable-version3 --pkg-config-flags=--static --disable-ffplay
	  libavutil      56. 22.100 / 56. 22.100
	  libavcodec     58. 35.100 / 58. 35.100
	  libavformat    58. 20.100 / 58. 20.100
	  libavdevice    58.  5.100 / 58.  5.100
	  libavfilter     7. 40.101 /  7. 40.101
	  libswscale      5.  3.100 /  5.  3.100
	  libswresample   3.  3.100 /  3.  3.100
	  libpostproc    55.  3.100 / 55.  3.100
	Input #0, mpegts, from 'b.ts':
	  Duration: 00:00:10.00, start: 11.400000
	  , bitrate: 1879 kb/s
	  Program 1 
	```

2. 想修改视频的 start 值， 经高人指点发现使用如下方式可以修改。
这条命令是将 mp4 文件转换为 ts 文件，同时使用 __initial_offset__ 参数指定第一个视频文件的 start 值。

	```shell
	ffmpeg -i 0.mp4 -vcodec copy -acodec aac -f segment -initial_offset 9.5 -segment_format mpegts out%d.ts
	```

3. 使用如上配置生成的 ts 文件播放后进度条就不跳跃了，问题得到解决。
